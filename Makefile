#
#
#
PACKAGE_NAME	= FMD FeeServer
PACKAGE		= fmdfeeserver
VERSION		= 1.1
RELEASE		= 20
DISTNAME	= $(PACKAGE)_bundled
DIM_V		= 19.11
FEESERVERXX_V	= 1.2
DCSC_V		= 0.5
RCUCE_V		= 1.3
NJOBS		= 
TEMPLATE	:= tmp-XXXXXX
TMP_INST	:= $(PWD)/tmp
TMP_DESTDIR	=  
TMP_PREFIX	=  $(TMP_INST)
DESTDIR		=  $(PWD)
JOBS		=  -j
BASE_CONF	=  --disable-docs
ifeq ($(VARIANT), arm)
PREFIX		:= /home/dcs
ARM_CONF	=  --host=arm-linux CC=arm-linux-gcc CXX=arm-linux-g++ --disable-strict
else 
PREFIX		:= /usr/local
endif
CONF_ARGS	= $(BASE_CONF) $(ARM_CONF)
PACKS		:= dcsc-$(DCSC_V)		\
		   dim-$(DIM_V)			\
		   feeserverxx-$(FEESERVERXX_V)	\
		   rcuce-$(RCUCE_V)		\
		   $(PACKAGE)-$(VERSION)	

%.done:%.tar.gz
	@echo -n "Extracting $* sources from $< ..."
	@tar -xzf $<
	@echo "done"
	@echo -n "Building $* ..."
	@(cd $* && 						\
	  set -e &&                                             \
	  echo "TMP_PREFIX=$(TMP_PREFIX)" ;			\
	  echo "TMP_INST=$(TMP_INST)" ;				\
	  echo "TMP_DESTDIR=$(TMP_DESTDIR)" ;			\
	  echo "CONF_ARGS=$(CONF_ARGS)" ;			\
	  ./configure 						\
		--prefix=$(TMP_PREFIX) 				\
		$(CONF_ARGS) 					\
		PATH=$(TMP_INST)/bin:$(TMP_PREFIX)/bin:${PATH}	\
	  &&							\
	  $(MAKE) $(JOBS)					\
	  &&							\
	  echo "Installing $@ in $(TMP_PREFIX)"			\
	  &&							\
	  $(MAKE) install DESTDIR=$(TMP_DESTDIR)) 
	@echo done 
	@touch $@ 

%.tar.gz:
	@set -e ; \
	d=`basename $@ | sed 's/-.*//'` ; \
	if test ! -d ../$$d ; then \
	  echo "Directory ../$$d does not exist" ; exit 1 ; fi ; \
	echo "Making $@ ... "  ; \
	(cd ../$$d && $(MAKE) dist >/dev/null) ; \
	cp ../$$d/$@ $@ 

%.spec:%.spec.in Makefile
	sed -e 's/@RPM_VERSION@/$(VERSION)/'		\
	    -e 's/@VERSION@/$(VERSION)/'		\
	    -e 's/@RELEASE@/$(RELEASE)/'		\
	    -e 's/@PACKAGE@/$(PACKAGE)/'		\
	    -e 's/@PACKAGE_NAME@/$(PACKAGE_NAME)/'	\
		< $< > $@

all:	$(PACKS:%=%.done)


clean:
	rm -rf  $(PACKS) 		\
		$(PACKS:%=%.done) 	\
		$(PACKS:%=%.log) 	\
		$(DESTDIR)$(PREFIX)	\
		$(PACKAGE).spec 	\
		*~ 

dist: $(PACKS:%=%.tar.gz) $(PACKAGE).spec Makefile
	mkdir $(DISTNAME)-$(VERSION)
	cp $^ $(DISTNAME)-$(VERSION)
	tar -czvf $(DISTNAME)-$(VERSION).tar.gz $(DISTNAME)-$(VERSION)
	rm -rf $(DISTNAME)-$(VERSION)

$(PACKAGE)-%.done:override TMP_PREFIX = $(PREFIX)
$(PACKAGE)-%.done:override TMP_DESTDIR = $(DESTDIR)
rcuce-%.done:	  override CONF_ARGS = $(BASE_CONF) $(ARM_CONF) --disable-strict
dim-%.done:	  override CONF_ARGS = $(BASE_CONF) $(ARM_CONF) --disable-jni --disable-java

update:$(PACKS:%=%.tar.gz)

distclean: clean
	rm -rf $(DISTNAME)-$(VERSION).tar.gz
	rm -rf $(PACKS) $(PACKS:%=%.tar.gz)
	rm -rf $(TMP_INST)

distcheck: dist
	tar -xzvf $(DISTNAME)-$(VERSION).tar.gz 
	(cd $(DISTNAME)-$(VERSION) && make)
	rm -rf $(DISTNAME)-$(VERSION)
	@echo "==============================================="
	@echo " $(DISTNAME)-$(VERSION).tar.gz  is ready"
	@echo "==============================================="

upload: dist
	scp $(DISTNAME)-$(VERSION).tar.gz lxplus.cern.ch:~/

#
# EOF
#
